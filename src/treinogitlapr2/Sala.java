/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package treinogitlapr2;

/**
 *
 * @author amartins
 */
public class Sala {
    private String idSala;
    private TipoSala tipo;
    
   public Sala(String id, TipoSala t)
   {
       this.idSala=id;
       this.tipo=t;
   }

    @Override
    public String toString() {
        return "Sala{" + "idSala=" + idSala + ", tipo=" + tipo + '}';
    }
      
}
