/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package treinogitlapr2;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author amartins
 */
public class Dia {
    private ArrayList<Aula> aulas;
    
    public Dia() {
        aulas = new ArrayList<Aula>();
    }
    
    public void add(Aula c) {
        aulas.add(c);
        Collections.sort(aulas);      // ineficiente, mas funciona
    }
    
    public String toString() {
        String s="";
        for(Aula a: aulas)
        {
             s=s+a.toString();
        }
        return s;
    }
}
