/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package treinogitlapr2;

/**
 *
 * @author amartins
 */

public class Aula implements Comparable<Aula> {  
    private TipoAula tipo;
    private Docente prof;
    private Turma turmas[];
    private UnidadeCurricular uc;
    private Sala sala;
    private int inicio, duracao; 
    
    public Aula (TipoAula t, Docente d, Turma vecT[], 
            UnidadeCurricular u, Sala s) {
        this.tipo = t;
        this.prof = d;
        this.uc = u;
        this.sala = s;
        for(int i = 0; i< vecT.length; i++)
            turmas[i] = vecT[i];
    }
    
    @Override
    public boolean equals(Object other){
        boolean result = false;
        if (other instanceof Aula) {
            Aula that = (Aula) other;
            if(this.tipo == that.tipo && this.prof.equals(that.prof) &&
                    this.uc.equals(that.uc) && this.sala.equals(that.sala) &&
                    this.inicio == that.inicio && this.duracao == that.duracao)
                result= true;
            else
                result = false;
        }
        return result;
    }
    
    @Override
    public int compareTo(Aula a) {     
        return this.inicio - a.inicio;
    }
    
    public int getduracao() {
        return duracao;
    }
    
    public String toString(){
        return "Trata-se da aula " + this.tipo + " do professor " + this.prof;
    }
}
