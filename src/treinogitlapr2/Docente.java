/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package treinogitlapr2;

/**
 *
 * @author amartins
 */
public class Docente implements Comparable<Docente> {
    private String sigla;
    private Horario horario;
    
    public Docente( String nome, int nDias){
        this.sigla = nome;
        horario = new Horario(nDias);
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla==null || sigla.isEmpty() ? "sem sigla" : sigla;
    }
    
   // ...
    
    @Override
    public boolean equals(Object other){
        if (other instanceof Docente) {
            Docente that = (Docente) other;
            return this.sigla.equals(that.sigla);
        }
        else
            return false;
    }


    @Override
    public int compareTo(Docente t) {
        return sigla.compareTo(t.sigla);
    }


    
    

    
    public String toString() {
        return "Sigla do professor: " + this.sigla;
    }

}
